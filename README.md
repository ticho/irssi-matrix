# Matrix client module for Irssi

This is still an early work in progress. The module so far can:
 - define a Matrix connection (with some manual Irssi config editing)
 - connect to a Matrix server
 - authenticate using either a password or an existing access token
 - recognize joined rooms and create channel windows in Irssi for each
 - poll for new events
 - show some basic room events like joins, leaves and text messages
 - send basic text messages to rooms
 - ...and that's it

## Installation

Requires:
 - Irssi headers (currently, you need the version from git master branch)
 - GLib-2 headers
 - matrix-glib from https://codeberg.org/ticho/matrix-glib (the official repo on github is outdated)

To build, use the usual autotools steps:
 - `./autogen.sh`
 - `./configure`
 - `make && sudo make install`

## Usage

Due to how networks and servers work in Irssi, the way to define a Matrix connection is a bit peculiar:

1. Load the module in Irssi
```
/load matrix
```

2. Add a network definition manually in your irssi configuration, in "chatnets" section. Something like:
```
matrix_foo = { type = "matrix"; };
```
3. Start Irssi and load the "matrix" module.
4. Create a server in the added network, using your Matrix ID as server address.
```
/server add -network matrix_foo @foo:example.org 0 secretpassword
```
...or, if you want to use access token from an existing Matrix login session...
```
/server add -network matrix_foo -access_token mysecrettoken @foo:example.org
```
5. Connect to the network, or to the server
```
/connect matrix_foo
```
...or...
```
/connect @foo:example.org
```

## Contact

I am "Ticho" on #irssi channel on Libera IRC network, or "@ticho:cyberdi.sk" on Matrix.
