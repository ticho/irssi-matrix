/*
 * Matrix client module for Irssi
 * Copyright (C) 2023 Andrej Kacian
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include <irssi/src/common.h>
#include <irssi/src/core/modules.h>
#include <irssi/src/core/signals.h>
#include <irssi/src/core/commands.h>

#include "matrix.h"
#include "matrix-servers-setup.h"

#define MODULE_NAME "fe-common/matrix"

MODULE_ABICHECK(fe_common_matrix)

static void
sig_matrix_server_add_fill(SERVER_SETUP_REC *_rec,
		GHashTable *optlist)
{
	gchar *value;
	MATRIX_SERVER_SETUP_REC *rec = (MATRIX_SERVER_SETUP_REC *)_rec;

	value = g_hash_table_lookup(optlist, "server_url");
	if (value != NULL) {
		g_free_and_null(rec->server_url);
		if (*value != '\0')
			rec->server_url = g_strdup(value);
	}

	value = g_hash_table_lookup(optlist, "access_token");
	if (value != NULL) {
		g_free_and_null(rec->access_token);
		if (*value != '\0')
			rec->access_token = g_strdup(value);
	}
}

void
fe_common_matrix_init(void)
{
	signal_add("server add fill", (SIGNAL_FUNC) sig_matrix_server_add_fill);
	command_set_options("server add", "-server_url -access_token");
	module_register("matrix", "fe-common");
}

void
fe_common_matrix_deinit(void)
{
	signal_remove("server add fill", (SIGNAL_FUNC) sig_matrix_server_add_fill);
}
