/*
 * Matrix client module for Irssi
 * Copyright (C) 2023 Andrej Kacian
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include <irssi/src/common.h>
#include <irssi/src/core/signals.h>

#include "matrix.h"
#include "module.h"
#include "matrix-servers.h"

void
matrix_sig_connected(MATRIX_SERVER_REC *server)
{
	g_debug("(server %s) server connected", server->tag);
}

void
matrix_session_init(void)
{
	signal_add("server connected", (SIGNAL_FUNC) matrix_sig_connected);
}

void matrix_session_deinit(void)
{
	signal_remove("server connected", (SIGNAL_FUNC) matrix_sig_connected);
}
