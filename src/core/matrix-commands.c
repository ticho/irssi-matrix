/*
 * Matrix client module for Irssi
 * Copyright (C) 2023 Andrej Kacian
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include <irssi/src/common.h>
#include <irssi/src/core/commands.h>
#include <irssi/src/core/window-item-def.h>

#include <matrix-message-emote.h>
#include <matrix-message-notice.h>
#include <matrix-event-room-message.h>

#include "matrix.h"
#include "matrix-commands.h"
#include "matrix-servers.h"
#include "matrix-channels.h"

static void
matrix_cmd_me(const char *data, MATRIX_SERVER_REC *server, WI_ITEM_REC *item)
{
	const gchar *target = window_item_get_target(item);
	GError *err = NULL;

	g_debug("(server %s) emote at target %s: %s", server->tag, target, data);

	CHANNEL_REC *_channel = channel_find(SERVER(server), target);
	if (_channel == NULL) {
		g_warning("Could not find channel %s to send emote", target);
		return;
	}

	MATRIX_CHANNEL_REC *channel = (MATRIX_CHANNEL_REC *)_channel;

	// Create a message object and a room event for it...
	MatrixMessageEmote *message = matrix_message_emote_new();
	matrix_message_base_set_body(MATRIX_MESSAGE_BASE(message), data);

	MatrixEventRoomMessage *event = matrix_event_room_message_new();
	matrix_event_room_message_set_message(event, MATRIX_MESSAGE_BASE(message));
	g_object_unref(message);

	// ...and send it.
	matrix_client_send(server->client, channel->room_id,
			MATRIX_EVENT_BASE(event), NULL, NULL, &err);
	if (err != NULL) {
		g_debug("Error when trying to send message: %s", err->message);
		g_error_free(err);
	} else {
		matrix_server_save_state(server);
	}
}


static void
matrix_cmd_notice(const char *data, MATRIX_SERVER_REC *server, WI_ITEM_REC *item)
{
	const gchar *target = window_item_get_target(item);
	GError *err = NULL;

	g_debug("(server %s) notice at target %s: %s", server->tag, target, data);

	CHANNEL_REC *_channel = channel_find(SERVER(server), target);
	if (_channel == NULL) {
		g_warning("Could not find channel %s to send emote", target);
		return;
	}

	MATRIX_CHANNEL_REC *channel = (MATRIX_CHANNEL_REC *)_channel;

	// Create a message object and a room event for it...
	MatrixMessageNotice *message = matrix_message_notice_new();
	matrix_message_base_set_body(MATRIX_MESSAGE_BASE(message), data);

	MatrixEventRoomMessage *event = matrix_event_room_message_new();
	matrix_event_room_message_set_message(event, MATRIX_MESSAGE_BASE(message));
	g_object_unref(message);

	// ...and send it.
	matrix_client_send(server->client, channel->room_id,
			MATRIX_EVENT_BASE(event), NULL, NULL, &err);
	if (err != NULL) {
		g_debug("Error when trying to send message: %s", err->message);
		g_error_free(err);
	} else {
		matrix_server_save_state(server);
	}
}


void
matrix_commands_init(void)
{
	command_bind_matrix("me", NULL, (SIGNAL_FUNC)matrix_cmd_me);
	command_bind_matrix("notice", NULL, (SIGNAL_FUNC)matrix_cmd_notice);
}

void matrix_commands_deinit(void)
{
	command_unbind("me", (SIGNAL_FUNC)matrix_cmd_me);
	command_unbind("notice", (SIGNAL_FUNC)matrix_cmd_notice);
}
