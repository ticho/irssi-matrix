#ifndef __MATRIX_EVENT_HANDLERS_H
#define __MATRIX_EVENT_HANDLERS_H

#include "matrix-servers.h"

void matrix_connect_events(MATRIX_SERVER_REC *server);

#endif // __MATRIX_EVENT_HANDLERS_H
