#ifndef __MATRIX_SERVERS_SETUP_H
#define __MATRIX_SERVERS_SETUP_H

#include <irssi/src/common.h>
#include <irssi/src/core/chat-protocols.h>
#include <irssi/src/core/servers-setup.h>

#define MATRIX_SERVER_SETUP(server) \
	PROTO_CHECK_CAST(SERVER_SETUP(server), MATRIX_SERVER_SETUP_REC, \
			chat_type, PROTOCOL_NAME)

#define IS_MATRIX_SERVER_SETUP(server) \
	(MATRIX_SERVER_SETUP(server) ? TRUE : FALSE)

typedef struct {
#include <irssi/src/core/server-setup-rec.h>

	gchar *server_url;
	gchar *access_token;
} MATRIX_SERVER_SETUP_REC;

void matrix_servers_setup_init(void);
void matrix_servers_setup_deinit(void);

#endif // __MATRIX_SERVERS_SETUP_H
