#include "module.h"

#include <irssi/src/common.h>
#include <irssi/src/fe-common/core/formats.h>

enum {
	MATRIXTXT_MODULE_NAME,

	MATRIXTXT_FILL_1,
	MATRIXTXT_OWN_NOTICE,
	MATRIXTXT_OWN_ACTION,

	MATRIXTXT_FILL_2,
	MATRIXTXT_NOTICE,
	MATRIXTXT_ACTION
};

extern FORMAT_REC matrix_formats[];
