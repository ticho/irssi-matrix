/*
 * Matrix client module for Irssi
 * Copyright (C) 2023 Andrej Kacian
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include <glib.h>

#include <irssi/src/common.h>
#include <irssi/src/core/signals.h>
#include <irssi/src/core/chat-protocols.h>
#include <irssi/src/core/nicklist.h>

#include <matrix-api.h>
#include <matrix-client.h>
#include <matrix-event-room-name.h>
#include <matrix-event-room-member.h>
#include <matrix-event-room-create.h>
#include <matrix-event-room-canonical-alias.h>
#include <matrix-event-room-topic.h>
#include <matrix-event-room-message.h>

#include "matrix-channels.h"
#include "matrix-servers.h"
#include "matrix-servers-setup.h"
#include "matrix-login.h"
#include "matrix-print-message-event.h"

static void
matrix_event_cb_room_name(
		MatrixClient *client,
		const gchar *room_id,
		JsonNode *raw_event,
		MatrixEventBase *matrix_event,
		void *user_data)
{
/*
	g_debug("(server %s) Got m.room.name event for room id %s: %s",
			MATRIX_SERVER(user_data)->tag,
			room_id ? room_id : "(null)",
			matrix_event_room_name_get_name(MATRIX_EVENT_ROOM_NAME(matrix_event))
	);
*/

	signal_emit("matrix room name", 3, user_data,
			room_id, matrix_event_room_name_get_name(MATRIX_EVENT_ROOM_NAME(matrix_event)));
}

static void
matrix_event_cb_room_member(
		MatrixClient *client,
		const gchar *room_id,
		JsonNode *raw_event,
		MatrixEventBase *_event,
		void *user_data)
{
	const gchar *nick;
	MatrixEventRoomMember *event = MATRIX_EVENT_ROOM_MEMBER(_event);
	const gchar *userid =
		matrix_event_room_member_get_user_id(event);
	MatrixRoomMembership mtype =
		matrix_event_room_member_get_membership(event);
	glong event_ts =
		matrix_event_room_get_origin_server_ts(MATRIX_EVENT_ROOM(_event));

/*
	g_debug("(server %s) Got m.room.member event for room %s, user %s (membership %d)",
			MATRIX_SERVER(user_data)->tag,
			room_id ? room_id : "(null)",
			userid, mtype
	);
*/

	signal_emit("matrix room member", 4,
			user_data,
			room_id ? room_id : "(null)",
			userid, mtype
	);

	MATRIX_CHANNEL_REC *chrec = matrix_channel_find_by_room_id(user_data, room_id);
	if (chrec == NULL)
		return;

	nick = matrix_event_room_member_get_display_name(event);

	if (nick == NULL)
		nick = userid;

	// Prepare timestamp for the event
	GDateTime *dt = g_date_time_new_from_unix_utc(event_ts/1000);
	gchar *ts = g_date_time_format_iso8601(dt);
	g_date_time_unref(dt);
	server_meta_stash(user_data, "time", ts);
	g_free(ts);

	// Send regular irssi event based on what actually happened
	NICK_REC *nrec;
	switch (mtype) {
		case MATRIX_ROOM_MEMBERSHIP_JOIN:
			nrec = g_new0(NICK_REC, 1);
			nrec->nick = g_strdup(nick);
			nicklist_insert(CHANNEL(chrec), nrec);
			nicklist_set_host(CHANNEL(chrec), nrec, userid);
			signal_emit("message join", 4, user_data,
					CHANNEL(chrec)->name, nrec->nick, nrec->host);

			break;
		case MATRIX_ROOM_MEMBERSHIP_LEAVE:
			nrec = matrix_channel_find_member_by_userid(CHANNEL(chrec), userid);
			if (nrec == NULL) {
//				g_debug("Could not find room member with userid %s in channel %s",
//						userid, CHANNEL(chrec)->name);
				break;
			}

			signal_emit("message part", 5, user_data,
					CHANNEL(chrec)->name, nrec->nick, nrec->host, "");
			nicklist_remove(CHANNEL(chrec), nrec);

			break;
		case MATRIX_ROOM_MEMBERSHIP_INVITE:
			break;
		case MATRIX_ROOM_MEMBERSHIP_BAN:
			break;
		case MATRIX_ROOM_MEMBERSHIP_KNOCK:
			break;
		case MATRIX_ROOM_MEMBERSHIP_UNKNOWN:
			break;
		default:
			break;
	}
	server_meta_clear_all(user_data);
}

static void
matrix_event_cb_room_create(
		MatrixClient *client,
		const gchar *room_id,
		JsonNode *raw_event,
		MatrixEventBase *matrix_event,
		void *user_data)
{
/*
	g_debug("(server %s) Got m.room.create event for room %s",
			MATRIX_SERVER(user_data)->tag,
			room_id ? room_id : "(null)"
	);
*/

	// TODO: add more event fields as signal params
	signal_emit("matrix room create", 2,
			user_data,
			room_id ? room_id : "(null)"
	);
}

static void
matrix_event_cb_room_canonical_alias(
		MatrixClient *client,
		const gchar *room_id,
		JsonNode *raw_event,
		MatrixEventBase *matrix_event,
		void *user_data)
{
	const gchar *alias = matrix_event_room_canonical_alias_get_canonical_alias(
			MATRIX_EVENT_ROOM_CANONICAL_ALIAS(matrix_event));
	SERVER_REC *server = SERVER(user_data);
	CHAT_PROTOCOL_REC *proto = chat_protocol_find_id(server->chat_type);

	g_debug("(server %s) Got m.room.canonical_alias event for room %s: %s",
			MATRIX_SERVER(user_data)->tag,
			room_id ? room_id : "(null)",
			alias
	);

	signal_emit("matrix room canonical_alias", 3,
			user_data,
			room_id ? room_id : "(null)",
			alias
	);

	MATRIX_CHANNEL_REC *room;
	if ((room = matrix_channel_find_by_room_id(MATRIX_SERVER(server), room_id)) != NULL) {
		if (room->canonical_alias == NULL || strcmp(room->canonical_alias, alias)) {
			g_warning("(server %s) Got m.canonical_alias event change for room %s: %s -> %s",
					MATRIX_SERVER(user_data)->tag, room->canonical_alias, alias);
		}
	}
}

static void
matrix_event_cb_room_topic(
		MatrixClient *client,
		const gchar *room_id,
		JsonNode *raw_event,
		MatrixEventBase *matrix_event,
		void *user_data)
{
	const gchar *topic = matrix_event_room_topic_get_topic(
			MATRIX_EVENT_ROOM_TOPIC(matrix_event));
	SERVER_REC *server = SERVER(user_data);
	glong event_ts =
		matrix_event_room_get_origin_server_ts(MATRIX_EVENT_ROOM(matrix_event));

/*
	g_debug("(server %s) Got m.room.topic event for room %s: %s",
			MATRIX_SERVER(user_data)->tag,
			room_id ? room_id : "(null)",
			topic
	);
*/

	MATRIX_CHANNEL_REC *chrec = matrix_channel_find_by_room_id(
			MATRIX_SERVER(server), room_id);
	if (chrec == NULL)
		return;

	const gchar *sender =
		matrix_event_room_get_sender(MATRIX_EVENT_ROOM(matrix_event));

	NICK_REC *nrec = matrix_channel_find_member_by_userid(CHANNEL(chrec), sender);
	if (nrec == NULL) {
/*
		g_warning("Could not find room member with userid %s in channel %s",
				sender, CHANNEL(chrec)->name);
*/
		return;
	}

	signal_emit("matrix room topic", 4,
			user_data,
			room_id ? room_id : "(null)",
			topic, sender
	);

	GDateTime *dt = g_date_time_new_from_unix_utc(event_ts/1000);
	gchar *ts = g_date_time_format_iso8601(dt);
	g_date_time_unref(dt);
	server_meta_stash(server, "time", ts);
	g_free(ts);

	g_free_not_null(chrec->topic);
	chrec->topic = g_strdup(topic);

	g_free_not_null(chrec->topic_by);
	chrec->topic_by = g_strdup(sender);

	chrec->topic_time = event_ts/1000;

	signal_emit("message topic", 5,
			server, CHANNEL(chrec)->name,
			topic,
			nrec->nick, nrec->host);

	server_meta_clear_all(user_data);

	signal_emit("channel topic changed", 1, chrec);
}

static void
matrix_event_cb_room_message(
		MatrixClient *client,
		const gchar *room_id,
		JsonNode *raw_event,
		MatrixEventBase *matrix_event,
		void *user_data)
{
	SERVER_REC *server = SERVER(user_data);
	MatrixMessageBase *msg = matrix_event_room_message_get_message(
			MATRIX_EVENT_ROOM_MESSAGE(matrix_event));
	const gchar *msgtype = matrix_message_base_get_message_type(msg);

	if (msgtype == NULL) {
		// This is probably a redaction event, ignore it for now.
		return;
	}

	g_debug("(server %s) Got m.room.message event for room %s (type %s)",
			MATRIX_SERVER(user_data)->tag,
			room_id ? room_id : "(null)",
			msgtype
	);

	signal_emit("matrix room message", 3,
			server,
			room_id ? room_id : "(null)",
			msg
	);

	glong event_ts =
		matrix_event_room_get_origin_server_ts(MATRIX_EVENT_ROOM(matrix_event));
	GDateTime *dt = g_date_time_new_from_unix_utc(event_ts/1000);
	gchar *ts = g_date_time_format_iso8601(dt);
	g_date_time_unref(dt);
	server_meta_stash(server, "time", ts);
	g_free(ts);

	matrix_print_message_event(MATRIX_SERVER(server),
			MATRIX_EVENT_ROOM_MESSAGE(matrix_event));

	server_meta_clear_all(user_data);
}

static void
matrix_cb_sync_started(MatrixClient *client, gpointer user_data)
{
	g_debug("(server %s) sync started", MATRIX_SERVER(user_data)->tag);

	signal_emit("matrix sync started", 1, user_data);
}

static void
matrix_cb_sync_ended(MatrixClient *client, gpointer user_data)
{
	g_debug("(server %s) sync ended", MATRIX_SERVER(user_data)->tag);

	signal_emit("matrix sync ended", 1, user_data);
}


void
matrix_connect_events(MATRIX_SERVER_REC *server)
{
	g_return_if_fail(server != NULL);
	g_return_if_fail(IS_MATRIX_SERVER(server));

	g_signal_connect(G_OBJECT(server->client), "login-finished",
			G_CALLBACK(matrix_cb_login_finished), server);

	g_signal_connect(G_OBJECT(server->client), "sync-started",
			G_CALLBACK(matrix_cb_sync_started), server);
	g_signal_connect(G_OBJECT(server->client), "sync-ended",
			G_CALLBACK(matrix_cb_sync_ended), server);

	matrix_client_connect_event(server->client,
			MATRIX_EVENT_TYPE_ROOM_NAME,
			matrix_event_cb_room_name, server, NULL);
	matrix_client_connect_event(server->client,
			MATRIX_EVENT_TYPE_ROOM_MEMBER,
			matrix_event_cb_room_member, server, NULL);
	matrix_client_connect_event(server->client,
			MATRIX_EVENT_TYPE_ROOM_CREATE,
			matrix_event_cb_room_create, server, NULL);
	matrix_client_connect_event(server->client,
			MATRIX_EVENT_TYPE_ROOM_CANONICAL_ALIAS,
			matrix_event_cb_room_canonical_alias, server, NULL);
	matrix_client_connect_event(server->client,
			MATRIX_EVENT_TYPE_ROOM_TOPIC,
			matrix_event_cb_room_topic, server, NULL);
	matrix_client_connect_event(server->client,
			MATRIX_EVENT_TYPE_ROOM_MESSAGE,
			matrix_event_cb_room_message, server, NULL);
}
