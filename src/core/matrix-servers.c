/*
 * Matrix client module for Irssi
 * Copyright (C) 2023 Andrej Kacian
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include <irssi/src/common.h>
#include <irssi/src/core/signals.h>

#include <matrix-http-client.h>
#include <matrix-event-room-message.h>
#include <matrix-message-text.h>

#include "module.h"
#include "matrix-servers.h"
#include "matrix-servers-setup.h"
#include "matrix-event-handlers.h"
#include "matrix-client-service-discovery.h"
#include "matrix-login.h"
#include "matrix-channels.h"

static gint
_string_compare_func(gconstpointer a, gconstpointer b)
{
	return g_strcmp0(a, b);
}

gboolean
matrix_server_is_in_room(MATRIX_SERVER_REC *server, const gchar *room_id)
{
	g_return_val_if_fail(server != NULL, FALSE);
	g_return_val_if_fail(room_id != NULL, FALSE);

	return (g_slist_find_custom(server->joined_rooms, room_id,
				_string_compare_func) != NULL);
}


gchar *
matrix_server_get_state_file_path(MATRIX_SERVER_REC *server)
{
	gchar *state_file = g_strdup_printf("matrix_%s.state",
			server->connrec->address);
	gchar *state_filepath = g_build_path(G_DIR_SEPARATOR_S,
					get_irssi_dir(), state_file, NULL);
	g_free(state_file);

	return state_filepath;
}


void
matrix_server_save_state(MATRIX_SERVER_REC *server)
{
	GError *err = NULL;
	gchar *state_filepath = matrix_server_get_state_file_path(server);

	g_debug("Saving state to file %s", state_filepath);
	matrix_client_save_state(server->client, state_filepath, &err);
	if (err != NULL) {
		g_warning("Failed to save matrix state to %s: %s",
				state_filepath, err->message);
		g_error_free(err);
	}

	g_free(state_filepath);
}


void
matrix_server_load_state(MATRIX_SERVER_REC *server)
{
	GError *err = NULL;
	gchar *state_filepath = matrix_server_get_state_file_path(server);

	g_debug("Loading state from file %s", state_filepath);
	matrix_client_load_state(server->client, state_filepath, &err);
	if (err != NULL) {
		g_warning("Failed to load matrix state from %s: %s",
				state_filepath, err->message);
		g_error_free(err);
	}

	g_free(state_filepath);
}


static void
matrix_server_send_message_func(SERVER_REC *_server, const char *target,
		const char *msg, int target_type)
{
	g_return_if_fail(_server != NULL);
	g_return_if_fail(target != NULL);
	g_return_if_fail(msg != NULL);

	GError *err = NULL;

	if (!IS_MATRIX_SERVER(_server))
		return;

	MATRIX_SERVER_REC *server = (MATRIX_SERVER_REC *)_server;

	g_debug("(server %s) (%d) %s: %s", server->tag, target_type, target, msg);

	CHANNEL_REC *_channel = channel_find(_server, target);
	if (_channel == NULL) {
		g_warning("Could not find channel %s to send message", target);
		return;
	}

	MATRIX_CHANNEL_REC *channel = (MATRIX_CHANNEL_REC *)_channel;

	// Create a message object and a room event for it...
	MatrixMessageText *message = matrix_message_text_new();
	matrix_message_base_set_body(MATRIX_MESSAGE_BASE(message), msg);

	MatrixEventRoomMessage *event = matrix_event_room_message_new();
	matrix_event_room_message_set_message(event, MATRIX_MESSAGE_BASE(message));
	g_object_unref(message);

	// ...and send it.
	matrix_client_send(server->client, channel->room_id,
			MATRIX_EVENT_BASE(event), NULL, NULL, &err);
	if (err != NULL) {
		g_debug("Error when trying to send message: %s", err->message);
		g_error_free(err);
	} else {
		matrix_server_save_state(server);
	}
}

SERVER_REC *
matrix_server_init_connect(SERVER_CONNECT_REC *_conn)
{
	MATRIX_SERVER_REC *server = NULL;
	MATRIX_SERVER_CONNECT_REC *conn = (MATRIX_SERVER_CONNECT_REC *)_conn;

	g_return_val_if_fail(IS_MATRIX_SERVER_CONNECT(conn), NULL);

	server = g_new0(MATRIX_SERVER_REC, 1);
	server->chat_type = MATRIX_PROTOCOL;

	server->connect_pid = -1;

	server->connrec = conn;
	server_connect_ref(_conn);

	server->client = NULL;

	gchar *state_filepath = matrix_server_get_state_file_path(server);

	g_debug("Looking at state file %s", state_filepath);

	if (g_file_test(state_filepath, G_FILE_TEST_EXISTS)) {
		if (g_file_test(state_filepath, G_FILE_TEST_IS_DIR)) {
			g_warning("State file %s is a directory, ignoring it.", state_filepath);
		} else {
			// Create the MatrixClient object with a dummy URL, for purpose of
			// loading the state file, and try loading the file.
			server->client = MATRIX_CLIENT(matrix_http_client_new("http://dummy"));

			GError *error = NULL;
			matrix_client_load_state(server->client, state_filepath, &error);

			if (error != NULL) {
				g_warning("Error while loading state file %s: %s",
						state_filepath, error->message);
				g_error_free(error);
				g_free(state_filepath);
				g_free(server);
				return NULL;
			}

			// Use data from the loaded state
			conn->server_url =
				g_strdup(matrix_http_api_get_base_url(MATRIX_HTTP_API(server->client)));
			conn->access_token =
				g_strdup(matrix_api_get_token(MATRIX_API(server->client)));
		}
	} else {
		g_debug("State file %s does not exist, not loading it", state_filepath);
	}

	g_free(state_filepath);

	if (conn->access_token == NULL &&
			conn->password == NULL) {
		g_debug("Neither server password, nor access_key is known for this server. Cannot connect.");
		g_free(server);
		return NULL;
	}

	conn->no_connect = 1;

	server->send_message = matrix_server_send_message_func;
	server->split_message = NULL;

	server_connect_init((SERVER_REC *)server);

	return (SERVER_REC *)server;
}

void
matrix_server_connect(SERVER_REC *_server)
{
	if (!IS_MATRIX_SERVER(_server))
		return;

	MATRIX_SERVER_REC *server = (MATRIX_SERVER_REC *)_server;
	MATRIX_SERVER_CONNECT_REC *connrec =
		(MATRIX_SERVER_CONNECT_REC *)server->connrec;

	if (connrec->server_url == NULL) {
		g_debug("explicit server_url not entered, doing client service discovery");

		MatrixServiceDiscoveryData *data = g_new0(MatrixServiceDiscoveryData, 1);

		if (!matrix_client_service_discovery_by_user_id(connrec->address, data)) {
			server_connect_failed(SERVER(server), "service discovery failed");
			return;
		}

		while (!data->ready)
			g_main_context_iteration(NULL, TRUE);

		if (data->result != MATRIX_SERVICE_DISCOVERY_RESULT_OK) {
			server_connect_failed(SERVER(server), "service discovery found nothing");
			return;
		}

		JsonNode *n;
		JsonObject *o;

		if ((n = json_object_get_member(data->json_data, "m.homeserver")) == NULL) {
			server_connect_failed(SERVER(server), "no m.homeserver found in service discovery reply");
			return;
		}

		if (!JSON_NODE_HOLDS_OBJECT(n)) {
			server_connect_failed(SERVER(server), "invalid service discovery reply format, expected a json object in m.homeserver");
			return;
		}

		o = json_node_get_object(n);

		if ((n = json_object_get_member(o, "base_url")) == NULL) {
			server_connect_failed(SERVER(server), "invalid service discovery reply format, expected base_url inside m.homeserver object");
			return;
		}

		if (!JSON_NODE_HOLDS_VALUE(n)) {
			server_connect_failed(SERVER(server), "invalid service discovery reply format, expected base_url to contain a value");
			return;
		}

		connrec->server_url = json_node_dup_string(n);

		json_object_unref(data->json_data);

		g_debug("got server url '%s' from service discovery", connrec->server_url);
	} else {
		g_debug("got explicit server_url '%s', skipping client service discovery",
				connrec->server_url);
	}

	// Create a fresh MatrixClient struct if it was not already done when loading
	// the state file.
	if (server->client == NULL)
		server->client = MATRIX_CLIENT(matrix_http_client_new(connrec->server_url));

	server->connect_time = time(NULL);

	matrix_connect_events(server);

	matrix_http_api_set_validate_certificate(MATRIX_HTTP_API(server->client), TRUE);

	GError *err = NULL;
	if (connrec->access_token != NULL) {
		g_debug("Using saved access_token");
		matrix_api_set_token(MATRIX_API(server->client), connrec->access_token);
		matrix_cb_login_finished(server->client, TRUE, server);
	} else {
		if (connrec->password == NULL) {
			server_connect_failed(SERVER(server), "Neither access_token nor password defined for this server in config, you need one of them to connect.");
			return;
		}

		g_debug("Logging in as %s", connrec->address);
		matrix_client_login_with_password(server->client, connrec->address,
				connrec->password, &err);
		if (err != NULL) {
			g_debug("FAIL:%s", err->message);
			server_connect_failed(SERVER(server), err->message);
			g_error_free(err);
			return;
		}
	}

	// The rest is happening in matrix_cb_login_finished() callback function
}

static int
matrix_ischannel_func(SERVER_REC *server, const char *data)
{
	// In Matrix, even one-on-one chats are functionally rooms
	return 1;
}

static const char *matrix_get_nick_flags_func(SERVER_REC *server)
{
	return "";
}

struct _MatrixJoinRoomCtx {
	MATRIX_SERVER_REC *server;
	gchar *room_alias;
};

static void
_internal_channels_join_cb(MatrixAPI *api,
		const gchar *content_type,
		JsonNode *json_content,
		GByteArray *raw_content,
		GError *err,
		gpointer user_data)
{
	g_return_if_fail(user_data != NULL);

	struct _MatrixJoinRoomCtx *ctx = (struct _MatrixJoinRoomCtx *)user_data;
	MATRIX_SERVER_REC *server = ctx->server;

	if (err) {
		g_warning("(server %s) Joining room %s failed: %s",
				server->tag, ctx->room_alias, err->message);
		g_error_free(err);
		return;
	}

	// Grab room_id from the server response
	JsonObject *root = json_node_get_object(json_content);
	JsonNode *node;

	if ((node =  json_object_get_member(root, "room_id")) == NULL) {
		g_warning("(server %s) Missing 'room_id' field in server response when trying to join %s.", server->tag, ctx->room_alias);
		g_free(ctx->room_alias);
		g_free(ctx);
		return;
	}

	gchar *room_id = json_node_dup_string(node);

	// Create the channel
	CHAT_PROTOCOL_REC *proto =
		chat_protocol_find_id(server->chat_type);
	CHANNEL_REC *chrec =
		proto->channel_create((SERVER_REC *)server, ctx->room_alias, ctx->room_alias, TRUE);
	MATRIX_CHANNEL(chrec)->room_id = room_id;

	g_free(ctx->room_alias);
	g_free(ctx);
}

static void
matrix_channels_join(SERVER_REC *_server, const char *arg, int automatic)
{
	// TODO: maybe support joining multiple rooms at once?
	// (/join #room1:foo #room2:foo ...)

	if (!IS_MATRIX_SERVER(_server))
		return;

	MATRIX_SERVER_REC *server = (MATRIX_SERVER_REC *)_server;

	if (matrix_channel_find_by_room_id_or_alias(server, arg) != NULL)
		return;

	g_debug("Calling join for '%s'", arg);

	struct _MatrixJoinRoomCtx *ctx = g_new0(struct _MatrixJoinRoomCtx, 1);
	ctx->server = server;
	ctx->room_alias = g_strdup(arg);

	GError *err = NULL;
	matrix_api_join_room_id_or_alias(MATRIX_API(server->client),
			arg, _internal_channels_join_cb, ctx, &err);

	if (err != NULL) {
		g_debug("FAIL:%s", err->message);
		g_error_free(err);
	}
}

static void
matrix_sig_server_connected(SERVER_REC *server)
{
	if (!IS_MATRIX_SERVER(server))
		return;

	server->ischannel = matrix_ischannel_func;
	server->get_nick_flags = matrix_get_nick_flags_func;
	server->channels_join = matrix_channels_join;
}

static void
matrix_sig_server_disconnected(SERVER_REC *_server)
{
	if (!IS_MATRIX_SERVER(_server))
		return;

	MATRIX_SERVER_REC *server = (MATRIX_SERVER_REC *)_server;

	GError *err = NULL;
	matrix_client_stop_polling(server->client, TRUE, &err);

	if (err != NULL) {
		g_warning("Error when trying to stop polling with MatrixClient: %s",
				err->message);
		g_error_free(err);
	}
}

void
matrix_servers_init(void)
{
	matrix_servers_setup_init();

	signal_add("server connected", (SIGNAL_FUNC)matrix_sig_server_connected);
	signal_add("server disconnected", (SIGNAL_FUNC)matrix_sig_server_disconnected);
}

void
matrix_servers_deinit(void)
{
	signal_remove("server connected", (SIGNAL_FUNC)matrix_sig_server_connected);
	signal_remove("server disconnected", (SIGNAL_FUNC)matrix_sig_server_disconnected);

	matrix_servers_setup_deinit();
}
