#ifndef __MATRIX_CHANNELS_H
#define __MATRIX_CHANNELS_H

#include <glib.h>
#include <gmodule.h>

#include <irssi/src/common.h>
#include <irssi/src/core/channels.h>
#include <irssi/src/core/nicklist.h>

#include "matrix-servers.h"

// Returns MATRIX_CHANNEL_REC if it's a Matrix channel, NULL otherwise.
#define MATRIX_CHANNEL(channel) \
	PROTO_CHECK_CAST(CHANNEL(channel), MATRIX_CHANNEL_REC, chat_type, PROTOCOL_NAME)

#define IS_MATRIX_CHANNEL(channel) \
	(MATRIX_CHANNEL(channel) ? TRUE : FALSE)

#define STRUCT_SERVER_REC MATRIX_SERVER_REC
typedef struct _MATRIX_CHANNEL_REC {
#include <irssi/src/core/channel-rec.h>

	gchar *room_id; /* Matrix room ID */
	gchar *canonical_alias; /* The canonical alias of the room */
} MATRIX_CHANNEL_REC;

MATRIX_CHANNEL_REC *matrix_channel_find_by_room_id(
		MATRIX_SERVER_REC *server,
		const gchar *room_id);

MATRIX_CHANNEL_REC *matrix_channel_find_by_room_id_or_alias(
		MATRIX_SERVER_REC *server,
		const gchar *arg);

NICK_REC *matrix_channel_find_member_by_userid(
		CHANNEL_REC *channel,
		const gchar *userid);

void matrix_channels_init(void);
void matrix_channels_deinit(void);

#endif // __MATRIX_CHANNELS_H
