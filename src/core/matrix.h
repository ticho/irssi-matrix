#ifndef __MATRIX_H
#define __MATRIX_H

#define PROTOCOL_NAME "matrix"
#define PROTOCOL_FULLNAME "Matrix Chat Protocol"
#define CHATNET_NAME "matrix"

#define MATRIX_PROTOCOL (chat_protocol_lookup(PROTOCOL_NAME))

#define CFG_SERVER_URL "server_url"
#define CFG_ACCESS_TOKEN "access_token"

#define SET_DEVICE_NAME "matrix_device_name"
#define DEFAULT_SET_DEVICE_NAME "Irssi-matrix"

#define SET_DEBUG "matrix_debug"
#define DEFAULT_SET_DEBUG FALSE

#endif // __MATRIX_H
