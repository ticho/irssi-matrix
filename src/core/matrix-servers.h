#ifndef __MATRIX_SERVERS_H
#define __MATRIX_SERVERS_H

#include <irssi/src/common.h>
#include <irssi/src/core/servers.h>
#include <irssi/src/core/chat-protocols.h>

#include <matrix-client.h>

#include "matrix.h"

#define MATRIX_SERVER_CONNECT(conn) \
	PROTO_CHECK_CAST(SERVER_CONNECT(conn), MATRIX_SERVER_CONNECT_REC, \
			chat_type, PROTOCOL_NAME)
#define IS_MATRIX_SERVER_CONNECT(conn) \
	(MATRIX_SERVER_CONNECT(conn) ? TRUE : FALSE)

typedef struct {
#include <irssi/src/core/server-connect-rec.h>

	MatrixClient *client;
	gchar *server_url;
	gchar *access_token;
	gboolean sync_active;
} MATRIX_SERVER_CONNECT_REC;

#define MATRIX_SERVER(conn) \
	PROTO_CHECK_CAST(SERVER(conn), MATRIX_SERVER_REC, \
			chat_type, PROTOCOL_NAME)
#define IS_MATRIX_SERVER(conn) \
	(MATRIX_SERVER(conn) ? TRUE : FALSE)

#define STRUCT_SERVER_CONNECT_REC MATRIX_SERVER_CONNECT_REC
typedef struct {
#include <irssi/src/core/server-rec.h>
	MatrixClient *client;
	GSList *joined_rooms;

	guint num_pending_rooms;
} MATRIX_SERVER_REC;

gboolean matrix_server_is_in_room(MATRIX_SERVER_REC *server, const gchar *room_id);

void matrix_server_save_state(MATRIX_SERVER_REC *server);
void matix_server_load_state(MATRIX_SERVER_REC *server);

SERVER_REC *matrix_server_init_connect(SERVER_CONNECT_REC *connrec);
void matrix_server_connect(SERVER_REC *server);

void matrix_servers_init(void);
void matrix_servers_deinit(void);

#endif // __MATRIX_SERVERS_H
