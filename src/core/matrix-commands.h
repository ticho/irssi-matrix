#ifndef __MATRIX_COMMANDS_H
#define __MATRIX_COMMANDS_H

#include <irssi/src/core/commands.h>

#include "module.h"

#define command_bind_matrix(cmd, section, signal) \
	command_bind_proto(cmd, MATRIX_PROTOCOL, section, signal)
#define command_bind_matrix_first(cmd, section, signal) \
	command_bind_proto_first(cmd, MATRIX_PROTOCOL, section, signal)
#define command_bind_matrix_last(cmd, section, signal) \
	command_bind_proto_last(cmd, MATRIX_PROTOCOL, section, signal)

/* Simply returns if server isn't for IRC protocol. Prints ERR_NOT_CONNECTED
 * error if there's no server or server isn't connected yet */
#define CMD_MATRIX_SERVER(server) \
	G_STMT_START { \
		if (server != NULL && !IS_MATRIX_SERVER(server)) \
			return; \
		if (server == NULL || !(server)->connected) \
			cmd_return_error(CMDERR_NOT_CONNECTED); \
	} G_STMT_END

void matrix_commands_init(void);
void matrix_commands_deinit(void);

#endif /* __MATRIX_COMMANDS_H */
