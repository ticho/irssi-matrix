#ifndef __MATRIX_LOGIN_H
#define __MATRIX_LOGIN_H

#include "matrix-servers.h"

void matrix_cb_login_finished(MatrixClient *client,
		gboolean success,
		gpointer user_data);

#endif // __MATRIX_LOGIN_H
