/*
 * Matrix client module for Irssi
 * Copyright (C) 2023 Andrej Kacian
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include "matrix-formats.h"

FORMAT_REC matrix_formats[] = {
	{ MODULE_NAME, "Matrix", 0 },

	{ NULL, "Your messages", 0 },
	{ "matrix_own_notice", "{ownnotice notice $0}$1", 2, { 0, 0 } },
	{ "matrix_own_action", "{ownaction $0}$1", 3, { 0, 0, 0 } },

	{ NULL, "Received messages", 0 },
	{ "matrix_notice", "{pubnotice $3 $0}$2", 4, { 0, 0, 0, 0 } },
	{ "matrix_action", "{pubaction $0}$1", 2, { 0, 0 } },

	{ NULL, NULL, 0, { 0 } }
};
