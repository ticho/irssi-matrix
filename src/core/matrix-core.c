/*
 * Matrix client module for Irssi
 * Copyright (C) 2023 Andrej Kacian
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include <irssi/src/common.h>
#include <irssi/src/core/modules.h>
#include <irssi/src/core/signals.h>
#include <irssi/src/core/settings.h>
#include <irssi/src/core/chat-protocols.h>
#include <irssi/src/core/chatnets.h>
#include <irssi/src/core/servers.h>
#include <irssi/src/core/servers-setup.h>
#include <irssi/src/core/channels-setup.h>

#include "module.h"
#include "matrix.h"
#include "matrix-commands.h"
#include "matrix-servers.h"
#include "matrix-servers-setup.h"
#include "matrix-channels.h"
#include "matrix-formats.h"

void matrix_session_init(void);
void matrix_session_deinit(void);

MODULE_ABICHECK(matrix_core)

static CHATNET_REC *
matrix_create_chatnet(void)
{
	return g_malloc0(sizeof(CHATNET_REC));
}

static SERVER_SETUP_REC *
matrix_create_server_setup(void)
{
	return g_malloc0(sizeof(MATRIX_SERVER_SETUP_REC));
}

static CHANNEL_SETUP_REC *
matrix_create_channel_setup(void)
{
	return g_malloc0(sizeof(CHANNEL_SETUP_REC));
}

static SERVER_CONNECT_REC *
matrix_create_server_connect(void)
{
	return g_malloc0(sizeof(MATRIX_SERVER_CONNECT_REC));
}

static void
matrix_destroy_server_connect(SERVER_CONNECT_REC *conn)
{
	MATRIX_SERVER_CONNECT_REC *mconn = MATRIX_SERVER_CONNECT(conn);

	if (mconn == NULL)
		return;

	g_free_not_null(mconn->server_url);

	// Erase the auth token from memory before freeing its memory.
	if (mconn->access_token != NULL) {
		memset(mconn->access_token, 0, strlen(mconn->access_token));
		g_free(mconn->access_token);
	}
}

static CHANNEL_REC *
matrix_channel_create(SERVER_REC *server, const gchar *name,
		const gchar *visible_name, int automatic)
{
	MATRIX_CHANNEL_REC *rec;

	g_return_val_if_fail(server != NULL, NULL);
	g_return_val_if_fail(IS_MATRIX_SERVER(server), NULL);
	g_return_val_if_fail(name != NULL, NULL);

	rec = g_new0(MATRIX_CHANNEL_REC, 1);

	channel_init((CHANNEL_REC *)rec, (SERVER_REC *)server,
			name, visible_name, automatic);

	return (CHANNEL_REC *)rec;
}


static void
_matrix_debug_log_func(const gchar *log_domain, GLogLevelFlags log_level,
		const gchar *message, gpointer user_data)
{
	if (settings_get_bool(SET_DEBUG)) {
		// TODO: output as CLIENTCRAP or something
	}
}

void
matrix_core_init (void)
{
	CHAT_PROTOCOL_REC *rec;

	rec = g_new0(CHAT_PROTOCOL_REC, 1);

	rec->name = PROTOCOL_NAME;
	rec->fullname = PROTOCOL_FULLNAME;
	rec->chatnet = CHATNET_NAME;
	rec->case_insensitive = FALSE;

	rec->create_chatnet = matrix_create_chatnet;
	rec->create_server_setup = matrix_create_server_setup;
	rec->create_channel_setup = matrix_create_channel_setup;
	rec->create_server_connect = matrix_create_server_connect;
	rec->destroy_server_connect = matrix_destroy_server_connect;
	rec->server_init_connect = matrix_server_init_connect;
	rec->server_connect = matrix_server_connect;
	rec->channel_create = matrix_channel_create;
	rec->query_create = NULL; // TODO

	g_log_set_handler("Matrix-GLib", G_LOG_LEVEL_DEBUG, _matrix_debug_log_func, NULL);

	matrix_session_init();
	matrix_servers_init();
	matrix_channels_init();
	matrix_commands_init();

	settings_add_str("matrix", SET_DEVICE_NAME, DEFAULT_SET_DEVICE_NAME);
	settings_add_bool("matrix", SET_DEBUG, DEFAULT_SET_DEBUG);

	settings_check();

	theme_register(matrix_formats);

	chat_protocol_register(rec);
	g_free(rec);

	module_register("matrix", "core");
}

void
matrix_core_deinit (void)
{
	signal_emit("chat protocol deinit", 1, chat_protocol_find(PROTOCOL_NAME));

	matrix_commands_deinit();
	matrix_channels_deinit();
	matrix_servers_deinit();
	matrix_session_deinit();

	chat_protocol_unregister(PROTOCOL_NAME);

	theme_unregister();
}
