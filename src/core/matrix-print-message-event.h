#ifndef __MATRIX_PRINT_MESSAGE_EVENT_H
#define __MATRIX_PRINT_MESSAGE_EVENT_H

#include "matrix-servers.h"

void matrix_print_message_event(MATRIX_SERVER_REC *server,MatrixEventRoomMessage *matrix_event);

#endif // __MATRIX_PRINT_MESSAGE_EVENT_H
