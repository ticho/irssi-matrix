/*
 * Matrix client module for Irssi
 * Copyright (C) 2023 Andrej Kacian
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include <glib.h>

#include "module.h"

#include <irssi/src/common.h>
#include <irssi/src/core/signals.h>
#include <irssi/src/core/chat-protocols.h>
#include <irssi/src/core/nicklist.h>
#include <irssi/src/core/levels.h>
#include <irssi/src/fe-common/core/printtext.h>

#include <matrix-api.h>
#include <matrix-client.h>
#include <matrix-event-room-name.h>
#include <matrix-event-room-member.h>
#include <matrix-event-room-create.h>
#include <matrix-event-room-canonical-alias.h>
#include <matrix-event-room-topic.h>
#include <matrix-event-room-message.h>

#include "matrix-channels.h"
#include "matrix-servers.h"
#include "matrix-servers-setup.h"
#include "matrix-login.h"
#include "matrix-formats.h"

void
matrix_print_message_event(
		MATRIX_SERVER_REC *server,
		MatrixEventRoomMessage *matrix_event)
{
	g_return_if_fail(server != NULL);
	g_return_if_fail(matrix_event != NULL);

	MatrixMessageBase *msg = matrix_event_room_message_get_message(matrix_event);
	glong event_ts = matrix_event_room_get_origin_server_ts(
			MATRIX_EVENT_ROOM(matrix_event));
	const gchar *msgtype = matrix_message_base_get_message_type(msg);
	const gchar *sender = matrix_event_room_get_sender(
			MATRIX_EVENT_ROOM(matrix_event));
	const gchar *room_id = matrix_event_room_get_room_id(
			MATRIX_EVENT_ROOM(matrix_event));
	const gboolean own =
		!strcmp(sender, matrix_api_get_user_id(MATRIX_API(server->client)));

	if (msgtype == NULL) {
		// This is probably a redaction event, ignore it.
		return;
	}

	MATRIX_CHANNEL_REC *chrec = matrix_channel_find_by_room_id(server, room_id);
	if (chrec == NULL)
		return;

	const gchar *nick, *host;

	NICK_REC *nrec = matrix_channel_find_member_by_userid(CHANNEL(chrec), sender);
	if (nrec == NULL) {
		g_debug("(server %s) Non-member %s in channel %s is sending a %s message",
				SERVER(server)->tag, sender, CHANNEL(chrec)->name, msgtype);
		// Use the user ID as both user and host for the message signal, as that's
		// the only thing we have available.
		nick = host = sender;
	} else {
		nick = nrec->nick;
		host = nrec->host;
	}

	if (!strcmp(msgtype, "m.text")) {
		if (own)
			signal_emit("message own_public", 3, server,
					matrix_message_base_get_body(msg),
					CHANNEL(chrec)->name);
		else
			signal_emit("message public", 5, server,
					matrix_message_base_get_body(msg),
					nick, host, CHANNEL(chrec)->name);

	} else if (!strcmp(msgtype, "m.emote")) {
		if (own)
			printformat(
				SERVER(server),
				CHANNEL(chrec)->name,
				MSGLEVEL_ACTIONS | MSGLEVEL_PUBLIC,
				MATRIXTXT_OWN_ACTION,
				nick,
				matrix_message_base_get_body(msg),
				CHANNEL(chrec)->name);
		else
			printformat(
				SERVER(server),
				CHANNEL(chrec)->name,
				MSGLEVEL_ACTIONS | MSGLEVEL_PUBLIC,
				MATRIXTXT_ACTION,
				nick,
				matrix_message_base_get_body(msg));

	} else if (!strcmp(msgtype, "m.notice")) {
		printformat(
				SERVER(server),
				CHANNEL(chrec)->name,
				MSGLEVEL_PUBNOTICES,
				own ? MATRIXTXT_OWN_NOTICE : MATRIXTXT_NOTICE,
				nick,
				CHANNEL(chrec)->name,
				matrix_message_base_get_body(msg),
				"");
	}
}
