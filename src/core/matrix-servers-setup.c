/*
 * Matrix client module for Irssi
 * Copyright (C) 2023 Andrej Kacian
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include <irssi/src/common.h>
#include <irssi/src/lib-config/iconfig.h>
#include <irssi/src/core/settings.h>
#include <irssi/src/core/signals.h>

#include <matrix-http-client.h>

#include "module.h"
#include "matrix.h"
#include "matrix-servers.h"
#include "matrix-servers-setup.h"

static void
sig_matrix_server_setup_read(
		MATRIX_SERVER_SETUP_REC *rec,
		CONFIG_NODE *node)
{
	g_return_if_fail(rec != NULL);
	g_return_if_fail(node != NULL);

	if (!IS_MATRIX_SERVER_SETUP(rec))
		return;

	rec->server_url = NULL;
	rec->access_token = NULL;
}

static void
sig_matrix_server_setup_saved(
		MATRIX_SERVER_SETUP_REC *rec,
		CONFIG_NODE *node)
{
	g_return_if_fail(rec != NULL);
	g_return_if_fail(node != NULL);

	if (!IS_MATRIX_SERVER_SETUP(rec))
		return;
}

static void
sig_matrix_server_setup_fill_server(SERVER_CONNECT_REC *_conn,
		SERVER_SETUP_REC *_set)
{
	g_return_if_fail(_conn != NULL);
	g_return_if_fail(_set != NULL);

	if (!IS_MATRIX_SERVER_SETUP(_set))
		return;

	MATRIX_SERVER_CONNECT_REC *conn = (MATRIX_SERVER_CONNECT_REC *)_conn;
	MATRIX_SERVER_SETUP_REC *set = (MATRIX_SERVER_SETUP_REC *)_set;
	g_return_if_fail(IS_MATRIX_SERVER_CONNECT(conn));
	g_return_if_fail(IS_MATRIX_SERVER_SETUP(set));

	if (set->server_url != NULL && conn->server_url == NULL)
		conn->server_url = g_strdup(set->server_url);
	if (set->access_token != NULL && conn->access_token == NULL)
		conn->access_token = g_strdup(set->access_token);
}

void
matrix_servers_setup_init(void)
{
	signal_add("server setup read", (SIGNAL_FUNC)sig_matrix_server_setup_read);
	signal_add("server setup saved", (SIGNAL_FUNC)sig_matrix_server_setup_saved);
	signal_add("server setup fill server", (SIGNAL_FUNC)sig_matrix_server_setup_fill_server);
}

void
matrix_servers_setup_deinit(void)
{
	signal_remove("server setup read", (SIGNAL_FUNC)sig_matrix_server_setup_read);
	signal_remove("server setup saved", (SIGNAL_FUNC)sig_matrix_server_setup_saved);
	signal_remove("server setup fill server", (SIGNAL_FUNC)sig_matrix_server_setup_fill_server);
}
