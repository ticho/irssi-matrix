/*
 * Matrix client module for Irssi
 * Copyright (C) 2023 Andrej Kacian
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include "matrix-channels.h"

MATRIX_CHANNEL_REC *
matrix_channel_find_by_room_id(
		MATRIX_SERVER_REC *server,
		const gchar *room_id)
{
	SERVER_REC *_server = SERVER(server);
	GSList *tmp;

	g_return_val_if_fail(room_id != NULL, NULL);

	for (tmp = server->channels; tmp != NULL; tmp = tmp->next) {
		MATRIX_CHANNEL_REC *chrec = tmp->data;

		if (chrec == NULL) {
			g_warning("(server %s) NULL channel!", server->tag);
			continue;
		}

		if (g_strcmp0(room_id, chrec->room_id) == 0)
			return chrec;

	}

	return NULL;
}


MATRIX_CHANNEL_REC *
matrix_channel_find_by_room_id_or_alias(
		MATRIX_SERVER_REC *server,
		const gchar *arg)
{
	SERVER_REC *_server = SERVER(server);
	GSList *tmp;

	g_return_val_if_fail(arg != NULL, NULL);

	for (tmp = server->channels; tmp != NULL; tmp = tmp->next) {
		MATRIX_CHANNEL_REC *chrec = tmp->data;

		if (chrec == NULL) {
			g_warning("(server %s) NULL channel!", server->tag);
			continue;
		}

		if (g_strcmp0(arg, chrec->room_id) == 0)
			return chrec;
		if (g_strcmp0(arg, chrec->canonical_alias) == 0)
			return chrec;
	}

	return NULL;
}


NICK_REC *
matrix_channel_find_member_by_userid(
		CHANNEL_REC *channel,
		const gchar *userid)
{
	NICK_REC *rec;
	GHashTableIter iter;

	g_return_val_if_fail(IS_CHANNEL(channel), NULL);
	g_return_val_if_fail(userid != NULL, NULL);

	g_hash_table_iter_init(&iter, channel->nicks);
	while (g_hash_table_iter_next(&iter, NULL, (void*)&rec)) {
		for (; rec != NULL; rec = rec->next) {
			if (!strcmp(userid, rec->host))
				return rec;
		}
	}

	return NULL;
}


void
matrix_channels_init(void)
{
}

void matrix_channels_deinit(void)
{
}
