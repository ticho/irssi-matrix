/*
 * Matrix client module for Irssi
 * Copyright (C) 2023 Andrej Kacian
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include <glib.h>

#include <irssi/src/common.h>
#include <irssi/src/core/signals.h>
#include <irssi/src/core/chat-protocols.h>

#include <matrix-api.h>
#include <matrix-client.h>
#include <matrix-event-room-name.h>
#include <matrix-event-room-member.h>
#include <matrix-event-room-create.h>
#include <matrix-event-room-canonical-alias.h>

#include "matrix-channels.h"
#include "matrix-servers.h"
#include "matrix-servers-setup.h"


struct _MatrixStateQueryCtx {
	MATRIX_SERVER_REC *server;
	gchar *room_id;
};


static void
_initial_get_canonical_alias_cb(MatrixAPI *api,
		const gchar *content_type,
		JsonNode *json_content,
		GByteArray *raw_content,
		GError *err,
		gpointer user_data)
{
	struct _MatrixStateQueryCtx *ctx = (struct _MatrixStateQueryCtx *)user_data;
	const gchar *channel_name = NULL;
	const gchar *canonical_alias = NULL;

	g_return_if_fail(ctx != NULL);

	MATRIX_SERVER_REC *server = MATRIX_SERVER(ctx->server);
	SERVER_REC *_server = SERVER(server);

	if (err != NULL) {
		gchar *tmp;
		switch (err->code) {
			case MATRIX_ERROR_M_FORBIDDEN:
				g_debug("(server %s) While getting m.room.canonical_alias events for room %s, server says we are not allowed: %s",
						_server->tag, ctx->room_id, err->message);
				g_free(ctx);
				g_error_free(err);
				return;

			case MATRIX_ERROR_M_NOT_FOUND:
				// This should be the only error code in this switch() block,
				// which does not end with a return.
				g_debug("(server %s) Room %s has no canonical alias, using its ID as Irssi channel name",
						_server->tag, ctx->room_id);
				channel_name = ctx->room_id;
				g_error_free(err);
				break;

			default:
				tmp = g_strdup_printf(
						"(server %s) Could not get canonical alias for room %s: %s (%d)",
						_server->tag, ctx->room_id, err->message, err->code);
				server_connect_failed(_server, tmp);
				g_free(tmp);
				g_free(ctx);
				g_error_free(err);
				return;
		}
	} else { // err == NULL
		if (!JSON_NODE_HOLDS_OBJECT(json_content)) {
			server_connect_failed(_server, "Invalid JSON received from server");
			g_free(ctx);
			g_error_free(err);
			return;
		}

		JsonObject *root = json_node_get_object(json_content);
		JsonNode *node;

		if ((node = json_object_get_member(root, "alias")) == NULL) {
			g_debug("(server %s) Missing 'alias' field from server's m.room.canonical_alias state response for room ID %s, using the ID as Irssi channel name instead",
					_server->tag, ctx->room_id);
			channel_name = ctx->room_id;
		} else {
			canonical_alias = channel_name = json_node_get_string(node);
		}
	}

	// Create the channel
	CHAT_PROTOCOL_REC *proto =
		chat_protocol_find_id(_server->chat_type);
	CHANNEL_REC *chrec =
		proto->channel_create(_server, channel_name, channel_name, TRUE);
	MATRIX_CHANNEL(chrec)->room_id = ctx->room_id;
	if (canonical_alias != NULL)
		MATRIX_CHANNEL(chrec)->canonical_alias = g_strdup(canonical_alias);

	g_free(ctx);

	if (--server->num_pending_rooms > 0) {
		// Not all requests for canonical alias are yet received, don't start polling.
		return;
	}

	// We have all initial info we need, start the polling loop!
	GError *pollerr = NULL;
	matrix_client_begin_polling(server->client, &pollerr);
	if (pollerr != NULL) {
		server_connect_failed(SERVER(server), pollerr->message);
		g_error_free(pollerr);
		return;
	}

	// Everything is fine, finish the connection process
	_server->connected = TRUE;
	server_connect_finished(_server);
	_server->real_connect_time = _server->connect_time = time(NULL);

}


static void
_to_list_func(JsonArray *array, guint index, JsonNode *node, gpointer user_data)
{
	GSList **jr = (GSList **)user_data;

	*jr = g_slist_prepend(*jr, json_node_dup_string(node));
}


static void
_initial_get_joined_rooms_cb(MatrixAPI *api,
		const gchar *content_type,
		JsonNode *json_content,
		GByteArray *raw_content,
		GError *err,
		gpointer user_data)
{
	MATRIX_SERVER_REC *server = (MATRIX_SERVER_REC *)user_data;
	SERVER_REC *_server = (SERVER_REC *)user_data;
	JsonNode *node;
	JsonObject *root;

	g_return_if_fail(server != NULL);

	if (!JSON_NODE_HOLDS_OBJECT(json_content)) {
		server_connect_failed(SERVER(server), "Invalid JSON received from server");
		return;
	}

	root = json_node_get_object(json_content);

	if ((node = json_object_get_member(root, "joined_rooms")) == NULL) {
		server_connect_failed(SERVER(server), "Missing 'joined_rooms' field from server response");
		return;
	}

	if (!JSON_NODE_HOLDS_ARRAY(node)) {
		server_connect_failed(SERVER(server), "Received 'joined_rooms' is not an array as expected");
		return;
	}

	json_array_foreach_element(json_node_get_array(node), _to_list_func, &server->joined_rooms);
	server->joined_rooms = g_slist_reverse(server->joined_rooms);

	g_debug("got %u joined rooms",
			(server->num_pending_rooms = g_slist_length(server->joined_rooms)));

	// Query m.room.canonical_alias for each of the joined rooms, so that
	// we can create corresponding channels in Irssi with correct names.
	GSList *tmp;
	for (tmp = server->joined_rooms; tmp != NULL; tmp = tmp->next) {
		struct _MatrixStateQueryCtx *ctx = g_new0(struct _MatrixStateQueryCtx, 1);
		ctx->server = server;
		ctx->room_id = tmp->data;

		GError *serr = NULL;
		matrix_api_get_room_state(MATRIX_API(server->client),
				tmp->data, "m.room.canonical_alias", NULL,
				_initial_get_canonical_alias_cb, ctx, &serr);
		if (serr != NULL) {
			server_connect_failed(SERVER(server), err->message);
			g_error_free(serr);
			g_free(ctx);
			return;
		}
	}
}


static void
_initial_get_profile_cb(MatrixAPI *api,
		const gchar *content_type,
		JsonNode *json_content,
		GByteArray *raw_content,
		GError *err,
		gpointer user_data)
{
	MATRIX_SERVER_REC *server = (MATRIX_SERVER_REC *)user_data;
	SERVER_REC *_server = (SERVER_REC *)user_data;

	g_return_if_fail(server != NULL);

	// "Disconnect" early if the server returned an error
	if (err != NULL) {
		server_connect_failed(SERVER(server), err->message);
		g_error_free(err);
		return;
	}

	// Get 'displayname' from the response
	if (!JSON_NODE_HOLDS_OBJECT(json_content)) {
		server_connect_failed(SERVER(server), "Invalid JSON received from server");
		return;
	}

	JsonObject *root = json_node_get_object(json_content);
	JsonNode *node;

	if ((node = json_object_get_member(root, "displayname")) == NULL) {
		server_connect_failed(SERVER(server), "Missing 'displayname' field from server's profile query response");
		return;
	}

	// Use the obtained displayname as our nickname locally
	const gchar *nick = json_node_get_string(node);
	server_change_nick(SERVER(server), nick);

	// Get list of joined rooms
	GError *jerr = NULL;
	matrix_api_get_joined_rooms(MATRIX_API(server->client),
			_initial_get_joined_rooms_cb, server, &jerr);
	if (jerr != NULL) {
		server_connect_failed(SERVER(server), jerr->message);
		g_error_free(jerr);
		return;
	}
}


static void
_initial_whoami_cb(MatrixAPI *api,
		const gchar *content_type,
		JsonNode *json_content,
		GByteArray *raw_content,
		GError *err,
		gpointer user_data)
{
	MATRIX_SERVER_REC *server = (MATRIX_SERVER_REC *)user_data;
	SERVER_REC *_server = (SERVER_REC *)user_data;
	MATRIX_SERVER_CONNECT_REC *connrec =
		(MATRIX_SERVER_CONNECT_REC *)server->connrec;

	g_return_if_fail(server != NULL);

	// "Disconnect" early if the server returned an error
	if (err != NULL) {
		server_connect_failed(_server, err->message);
		g_error_free(err);
		return;
	}

	// Get 'user_id' from the response
	if (!JSON_NODE_HOLDS_OBJECT(json_content)) {
		server_connect_failed(_server, "Invalid JSON received from server");
		return;
	}

	JsonObject *root = json_node_get_object(json_content);
	JsonNode *node;

	if ((node = json_object_get_member(root, "user_id")) == NULL) {
		server_connect_failed(_server, "Missing 'user_id' field from server's whoami response");
		return;
	}

	const gchar *user_id = json_node_get_string(node);
	if (strcmp(user_id, connrec->address)) {
		server_connect_failed(_server, "Saved token belongs to another user ID");
		return;
	}

	// Request our profile, so that we can get 'displayname' and use it
	// as this connection's nickname locally.
	// For connections using existing access_token, this also serves as a
	// test whether the token is valid and the connection can be deemed
	// active.
	GError *perr = NULL;
	matrix_api_get_profile(MATRIX_API(server->client), server->connrec->address,
			_initial_get_profile_cb, server, &perr);
	if (perr != NULL) {
		server_connect_failed(_server, perr->message);
		g_error_free(perr);
		return;
	}

	// Rest is happening in _initial_get_profile_cb
}


void
matrix_cb_login_finished(MatrixClient *client, gboolean success, gpointer user_data)
{
	MATRIX_SERVER_REC *server = MATRIX_SERVER(user_data);
	GError *err = NULL;

	g_debug("(server %s) login finished: %s", server->tag,
			success ? "success" : "failure");

	if (!success)
		return; // The error is handled in the connect function already

	// Save the access_token in config, so that we do not lose this
	// login session and can get back to it in future.
	const gchar *token = matrix_api_get_token(MATRIX_API(client));

	if (token == NULL) {
		g_warning("Could not get access token from SDK, login will not persist.");
	} else {
		MATRIX_SERVER_SETUP_REC *setup = MATRIX_SERVER_SETUP(
				server_setup_find(
					server->connrec->address,
					server->connrec->port,
					server->connrec->chatnet
				)
		);

		if (setup == NULL) {
			g_warning("Could not identify Irssi server definition to save the access token for, login will not persist.");
		} else {
			if (setup->access_token != NULL)
				g_free_and_null(setup->access_token);
			setup->access_token = g_strdup(token);
			server_setup_add(SERVER_SETUP(setup));

			matrix_server_save_state(server);
		}
	}

	// Verify that we have the correct account
	err = NULL;
	matrix_api_whoami(MATRIX_API(server->client), _initial_whoami_cb, server, &err);
	if (err != NULL) {
		server_connect_failed(SERVER(server), err->message);
		g_error_free(err);
		return;
	}

	// Rest is happening in _initial_whoami_cb
}
